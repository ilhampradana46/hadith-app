from django.core.management.base import BaseCommand
from hadith_app.models import Hadiths
import json

"""
This file will execute by CI/CD automaticaly for populating hadith data
"""

class Command(BaseCommand):
    help = 'Inserts a JSON file into the database.'

    def add_arguments(self, parser):
        parser.add_argument('json_file', type=str, help='The path to the JSON file.')

    def handle(self, *args, **options):
        with open(options['json_file'], 'r', encoding='utf-8') as f:
            json_data = json.load(f)
        insert_hadith = json_data["list_hadith"]
        instance = [Hadiths(id_hadith=item['number'], arabic=item['arab'], bahasa=item['id'], ulama=1) for item in insert_hadith[1:len(insert_hadith)]]
        Hadiths.objects.bulk_create(instance)
        return print(len(insert_hadith))
        

