from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
class Hadiths(models.Model):
    id_hadith = models.AutoField(
        primary_key=True
    )

    arabic = models.TextField(
        null=False,
        blank=False,
        db_collation='utf8mb4_unicode_ci'
    )

    bahasa = models.TextField(
        null=False,
        blank=False
    )

    ulama = models.IntegerField(
        max_length=1000,
        null=False,
    )

    creation_date = models.DateTimeField(
        auto_now_add=True,
        null=False,
        blank=False
    )

    last_updated = models.DateTimeField(
        auto_now=True,
        null=False,
        blank=False
    )

class Meta:
    db_table = 'Hadiths'