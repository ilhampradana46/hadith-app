from rest_framework import serializers
from .models import Hadiths

class HadithSerializers(serializers.ModelSerializer):
    
    class Meta:
        model = Hadiths
        fields = ['id_hadith', 'arabic', 'bahasa', 'ulama']