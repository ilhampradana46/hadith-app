from django.urls import path
from .views import HadithAPI

urlpatterns = [
    path('', HadithAPI.as_view()),
]