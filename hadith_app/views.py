from rest_framework import status
from rest_framework.views import APIView, Response
from .models import Hadiths
from .serializers import HadithSerializers

class HadithAPI(APIView):
    
    def get(self, request):
        all_hadith = Hadiths.objects.all()
        hadith_json = HadithSerializers(all_hadith, many=True).data
        response_data = {"datas" : hadith_json}
        return Response(response_data, status=status.HTTP_200_OK)
    
    def post(self, request):
        insert = {
            'id_hadith' : request.data.get('id_hadith'),
            'arabic' : request.data.get('arabic'),
            'bahasa' : request.data.get('bahasa'),
            'ulama' : request.data.get('ulama')
        }
        Hadiths.objects.create(**insert)
        return Response({"res" : "response created", "data": insert}, status=status.HTTP_200_OK)